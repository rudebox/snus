<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//text
	$text = get_field('page_text');

	//badge img
	$badge = get_field('badge');

	//product img
	$product = get_field('page_product');

	//product img
	$bag = get_field('page_bag');
?>

<?php if (is_front_page() ) : ?>
<section class="page__hero page__hero--front">
<?php else : ?>
<section class="page__hero">
<?php endif; ?>
	<div class="wrap hpad">
		<div class="row page__row">
			<div class="col-sm-10 page__col">
				<h1 class="page__title"><?php echo $title; ?></h1>
				<p class="page__text wow fadeInLeft"><?php echo $text; ?></p>
			</div>	

			<?php if ($badge) : ?>
			<div class="page__badge col-sm-2 wow bounceIn">
				<img class="page__badge" src="<?php echo esc_url($badge['url']); ?>" alt="<?php echo esc_attr($badge['alt']); ?>">
			</div>
			<?php endif; ?>

			<?php if ($product) : ?>
			<?php if (is_front_page() ) : ?>
			<div class="page__product page__product--front col-sm-6">
			<?php else : ?>
			<div class="page__product col-sm-6">
			<?php endif; ?>
				<img  src="<?php echo esc_url($product['url']); ?>" alt="<?php echo esc_attr($product['alt']); ?>">
			</div>
			<?php endif; ?>

			<?php if ($bag) : ?>
			<div class="page__bag col-sm-6">
				<img  src="<?php echo esc_url($bag['url']); ?>" alt="<?php echo esc_attr($bag['alt']); ?>">
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>